function customLemonTabs(selector){
  	/* Tab */
		jQuery(selector+"> .tab-container .tab-content").hide(); //Hide all tab content
		jQuery(selector+"> .checkable-element .tab-label:first-child .checkable-item").addClass("active").show(); //Activate first tab
		jQuery(selector+"> .checkable-element .tab-label:first-child input:radio").attr("checked", ""); // Checked the fiest tab radio
		jQuery(selector+"> .tab-container .tab-content:first-child").show(); //Show first tab content

	  	  //On Click Event
  	jQuery(selector+" > .checkable-element .tab-label .checkable-item").click(function() {
	    jQuery(selector+" > .checkable-element .tab-label .checkable-item").removeClass("active"); //Remove any "active" class

	    // Add active class on clicked element 
	    jQuery(this).addClass("active")
	    jQuery(selector+"> .tab-container .tab-content").hide(); //Hide all tab content
	    
	    // //Find the href attribute value to identify the active tab + content
	    var activeTab = jQuery(this).prev().val(); 
	    jQuery('.tab-container #' + activeTab).fadeIn(); //show the active ID content with Fade in
	    
	    // return false;
		});
}